const { sequelize, Image, Video, Comment } = require("./models");
const express = require("express");
const db = require('./models');
const port = 5000;

const app = express();
app.use(express.json());

app.post('/images', async (req, res) => {
    const {title, url} = req.body;
    try {
        const images = await Image.create ({title, url});
        return res.json(images);
    } catch (error) {
        console.log(error);
        return res.json(error);
    }
})

app.post('/videos', async (req, res) => {
    const {title, test} = req.body;
    try {
        const videos = await Video.create ({title, test});
        return res.json(videos);
    } catch (error) {
        console.log(error);
        return res.json(error);
    }
})

app.post('/image/:id/comments', async (req, res) => {
    const id = req.params.id;
    const {title, commentableType} = req.body;
    try {
        const image = await Image.findOne({
            where: {
                id,
            }
        })
        const comments = await Comment.create ({title, commentableId: image.id, commentableType});
        return res.json(comments);
    } catch (error) {
        console.log(error);
        return res.json(error);
    }
})

app.post('/video/:id/comments', async (req, res) => {
    const id = req.params.id;
    const {title, commentableType} = req.body;
    try {
        const video = await Video.findOne ({where:{id}});
        const comments = await Comment.create({title, commentableId:video.id, commentableType})
        return res.json(comments);
    } catch (error) {
        console.log(error);
        return res.json(error);
    }
})

app.get ('/image/:id/comments', async(req, res) => {
    const id = req.params.id;
    try {
        const image = await Image.findOne({
            where: {
                id
            },
            include: [Comment]
        })
        return res.json(image);
    } catch (error) {
        console.log(error);
        return res.json(error);
    }
})

app.get ('/video/:id/comments', async(req, res) => {
    const id = req.params.id;
    try {
        const video = await Video.findOne({
            where: {
                id
            },
            include: [Comment]
        })
        return res.json(video);
    } catch (error) {
        console.log(error);
        return res.json(error);
    }
})

app.listen({port: 5000}, async() => {
    console.log('Server hosted on localhost:5000');
    await sequelize.sync();
    console.log('Database Connected');
})

